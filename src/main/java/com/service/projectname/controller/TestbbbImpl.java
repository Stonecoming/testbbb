package com.service.projectname.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-03-12T10:10:37.566+08:00")

@RestSchema(schemaId = "testbbb")
@RequestMapping(path = "/projectName", produces = MediaType.APPLICATION_JSON)
public class TestbbbImpl {

    @Autowired
    private TestbbbDelegate userTestbbbDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userTestbbbDelegate.helloworld(name);
    }

}
